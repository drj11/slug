package main

import (
	"bufio"
	"encoding/xml"
	"fmt"
	"io"
	"log"
	"math"
	"os"
	"strings"
)

// XML Models
type SVG struct {
	XMLName xml.Name `xml:"http://www.w3.org/2000/svg svg"`
	ViewBox string   `xml:"viewBox,attr"`
	Defs    Defs     `xml:"defs>g"`
	Groups  []G      `xml:"g"`
}
type Defs struct {
	Symbols []Symbol `xml:"symbol"`
}
type G struct {
	Id string `xml:"id,attr,omitempty"`
	// Rect (not needed?)
	Name      string `xml:"name,attr,omitempty"`
	Style     string `xml:"style,attr,omitempty"`
	Transform string `xml:"transform,attr,omitempty"`
	Rect      []Rect `xml:"rect"`
	Paths     []Path `xml:"path"`
	Groups    []G    `xml:"g"`
	Uses      []Use  `xml:"use"`
}
type Path struct {
	Class     string `xml:"class,attr"`
	Id        string `xml:"id,attr"`
	D         string `xml:"d,attr"`
	Transform string `xml:"transform,attr"`
}
type Rect struct {
	X      string `xml:"x,attr"`
	Y      string `xml:"y,attr"`
	Width  string `xml:"width,attr"`
	Height string `xml:"height,attr"`
	Style  string `xml:"style,attr,omitempty"`
}
type Symbol struct {
	Id       string `xml:"id,attr"`
	Overflow string `xml:"overflow,attr"`
	Path     Path   `xml:"path"`
}
type Use struct {
	Href string `xml:"http://www.w3.org/1999/xlink href,attr"`
	X    string `xml:"x,attr"`
	Y    string `xml:"y,attr"`
}

func main() {
	inp := bufio.NewReader(os.Stdin)

	u := &SVG{}
	svgs := []*SVG{}
	for {
		svg, err := ReadSVG(inp)
		if err == io.EOF {
			break
		}
		if err != nil {
			log.Fatal(err)
		}

		svgs = append(svgs, svg)
	}

	for i, svg := range svgs {
		PrefixID(svg, "slug"+fmt.Sprint(i)+"-")
		SVGAddOnRight(u, svg)
	}

	obs, _ := xml.MarshalIndent(u, "", "  ")
	os.Stdout.Write(obs)
}

// add a prefix to all the ids.
func PrefixID(svg *SVG, prefix string) {
	for i := range svg.Defs.Symbols {
		svg.Defs.Symbols[i].Id = prefix + svg.Defs.Symbols[i].Id
	}
	PrefixGroupsRecurse(svg.Groups, prefix)
}

// Helper for PrefixID
func PrefixGroupsRecurse(gs []G, prefix string) {
	for i := range gs {
		id := gs[i].Id
		if id != "" {
			gs[i].Id = prefix + gs[i].Id
		}
		for j := range gs[i].Uses {
			href := gs[i].Uses[j].Href
			if strings.HasPrefix(href, "#") {
				gs[i].Uses[j].Href = "#" + prefix + href[1:]
			}
		}
		PrefixGroupsRecurse(gs[i].Groups, prefix)
	}
}

// To u add the SVG a.
func SVGAddOnRight(u, a *SVG) {
	// Useful to remind ourselves of the SVG viewBox attribute:
	// https://developer.mozilla.org/en-US/docs/Web/SVG/Attribute/viewBox
	// xmin ymin width height

	// The width of u, which will become the x-axis
	// translation for each of the groups in a.
	var uWidth float64
	if u.ViewBox == "" {
		u.ViewBox = a.ViewBox
	} else {
		var f float64
		var uHeight float64
		fmt.Sscanf(u.ViewBox, "%f%f%f%f", &f, &f, &uWidth, &uHeight)
		var aHeight, aWidth float64
		fmt.Sscanf(a.ViewBox, "%f%f%f%f", &f, &f, &aWidth, &aHeight)
		uHeight = math.Max(uHeight, aHeight)
		newWidth := uWidth + aWidth
		u.ViewBox = fmt.Sprint(0, 0, newWidth, uHeight)
	}
	if u.XMLName == (xml.Name{}) {
		u.XMLName = a.XMLName
	}
	u.Defs.Symbols = append(u.Defs.Symbols, a.Defs.Symbols...)

	// For translate operator in translate attribute, see
	// https://developer.mozilla.org/en-US/docs/Web/SVG/Attribute/transform
	transform := fmt.Sprintf("translate(%f 0)", uWidth)

	for i := range a.Groups {
		a.Groups[i].Transform = transform
	}
	u.Groups = append(u.Groups, a.Groups...)
}

// Read SVG from r, decoding it to a model.
func ReadSVG(r io.Reader) (*SVG, error) {

	svg := SVG{}
	decoder := xml.NewDecoder(r)
	err := decoder.Decode(&svg)
	if err != nil {
		return nil, err
	}

	return &svg, nil
}
