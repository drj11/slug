# slug

Combined SVG images horizontally to make a slug.

The SVG images should originate from `hb-view`.

Typical usage:

    panel 'P&Q' *.ttf |
    slug |
    rsvg-convert | kitty icat

## Developer Notes

Much of the code has been hurriedly copied from `gley`.
The code should be unified.

# END
